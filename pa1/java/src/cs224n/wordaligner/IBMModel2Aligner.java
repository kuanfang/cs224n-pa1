package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;
import java.util.Set;

/**
 * Simple word alignment baseline model that maps source positions to target 
 * positions along the diagonal of the alignment grid.
 * 
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * 
 * @author Dan Klein
 * @author Spence Green
 */
public class IBMModel2Aligner implements WordAligner {

  private static final long serialVersionUID = 1315751943476440515L;
  private static double IBM1_ITER_THRESHOLD = 5E-4;
  private static double IBM2_ITER_THRESHOLD = 5E-4;
  private static long TIME_LIMIT = 5000000;   // To prevent time out, set to infinity in extra credit 1.
  
  // TODO: Use arrays or Counters for collecting sufficient statistics
  // from the training data.
  private CounterMap<String, String> targetSourceProb; // Pr(f|e)
  private CounterMap<String, Integer> alignProb; // Pr(a)
  private long startTime; // Compute running time. Also prevent time out.

  public Alignment align(SentencePair sentencePair) {
    // Placeholder code below. 
    // TODO Implement an inference algorithm for Eq.1 in the assignment
    // handout to predict alignments based on the counts you collected with train().
    Alignment alignment = new Alignment();

    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sourceWords.size();
    int numTargetWords = targetWords.size();
    for (int sourceIdx = 0; sourceIdx < numSourceWords; sourceIdx++) {
      String source = sourceWords.get(sourceIdx);
      String alignKey = makeAlignKey(sourceIdx, sourceWords.size(), targetWords.size());
      double max = 0.0;
      int maxIdx = -1;
      for(int targetIdx = 0; targetIdx < numTargetWords; targetIdx++){
        String target = targetWords.get(targetIdx);
        double score = targetSourceProb.getCount(target, source) * alignProb.getCount(alignKey, targetIdx);
        if (max < score) {
          max = score;
          maxIdx = targetIdx;
        }
      }
      if (maxIdx >= 0) {
        alignment.addPredictedAlignment(maxIdx, sourceIdx);
      }
    }
   
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
    
    startTime = System.currentTimeMillis();
    
    // First initialize
    simpleInitializeTargetSourceProb(trainingPairs);
    
    // initialize Pr(fi|ej,ai=j)
    IBM1InitializeTargetSourceProb(trainingPairs);
 
    // initialize Pr(ai=j)
    // simpleInitializeAlignProb(trainingPairs);
    advanceInitializeAlignProb(trainingPairs);
     
    // EM
    IBM2(trainingPairs);
    
    // showAlignProb();
    long trainingTime = System.currentTimeMillis() - startTime;
    System.out.println("Training time: " + String.valueOf(trainingTime) + " ms");
  }
  
  public void simpleInitializeTargetSourceProb(List<SentencePair> trainingPairs) {
    targetSourceProb = new CounterMap<String,String>();
    for (SentencePair pair : trainingPairs){
      List<String> targetWords = pair.getTargetWords();
      List<String> sourceWords = pair.getSourceWords();
      for(String source : sourceWords){
        for(String target : targetWords){
          targetSourceProb.incrementCount(target, source, 1.0);
        }
        targetSourceProb.incrementCount(NULL_WORD, source, 1.0);
      }
    }
  }
  
  public void IBM1InitializeTargetSourceProb(List<SentencePair> trainingPairs) {
    CounterMap<String,String> targetSourceProbPrev = new CounterMap<String,String>();
    int iteration = 0;
    while (probDiff(targetSourceProbPrev, targetSourceProb) > IBM1_ITER_THRESHOLD) {      
      targetSourceProbPrev = targetSourceProb;
      CounterMap<String,String> probCount = new CounterMap<String,String>();
      for (SentencePair pair : trainingPairs){
        List<String> targetWords = pair.getTargetWords();
        List<String> sourceWords = pair.getSourceWords();
        for(int sourceIdx = 0; sourceIdx < sourceWords.size(); sourceIdx++){
          String source = sourceWords.get(sourceIdx);
          Counter<String> tmpCount = new Counter<String>();
          double total = 0.0;
          for (String target: targetWords) {
            tmpCount.incrementCount(target, targetSourceProb.getCount(target, source));
            total += targetSourceProb.getCount(target, source);
          }
          tmpCount.incrementCount(NULL_WORD, targetSourceProb.getCount(NULL_WORD, source));
          total += targetSourceProb.getCount(NULL_WORD, source);
          for (String target: tmpCount.keySet()) {
            probCount.incrementCount(target, source, tmpCount.getCount(target) / total);
          }
        }
      } 
      
      targetSourceProb = Counters.conditionalNormalize(probCount);
      
      iteration++;
      System.out.println("IBM1 " + String.valueOf(iteration) + " iter: " + String.valueOf(probDiff(targetSourceProbPrev, targetSourceProb)));
    } 
  }
  
  public void simpleInitializeAlignProb(List<SentencePair> trainingPairs) {
    alignProb = new CounterMap<String,Integer>();
    for (SentencePair pair : trainingPairs){
      for (int sourceIdx = 0; sourceIdx < pair.getSourceWords().size(); sourceIdx++) {
        for (int targetIdx = -1; targetIdx < pair.getTargetWords().size(); targetIdx++) {
          alignProb.setCount(makeAlignKey(sourceIdx, pair.getSourceWords().size(), pair.getTargetWords().size()), targetIdx, 1.0);
        }
      }
    }
    alignProb = Counters.conditionalNormalize(alignProb);
  }
  
  public void advanceInitializeAlignProb(List<SentencePair> trainingPairs) {
    alignProb = new CounterMap<String,Integer>();
    for (SentencePair pair : trainingPairs){
      for (int sourceIdx = 0; sourceIdx < pair.getSourceWords().size(); sourceIdx++) {
        for (int targetIdx = -1; targetIdx < pair.getTargetWords().size(); targetIdx++) {
          double predictedScore;
          if (targetIdx == -1) {
            predictedScore = 1;
          } else {
            double ratio = (double)(sourceIdx+1) / pair.getSourceWords().size() / ((double)(targetIdx+1) / pair.getTargetWords().size());
            predictedScore = ratio>1? 1/ratio: ratio;
          }
          alignProb.setCount(makeAlignKey(sourceIdx, pair.getSourceWords().size(), pair.getTargetWords().size()), targetIdx, predictedScore);
        }
      }
    }
    alignProb = Counters.conditionalNormalize(alignProb);
  }
  
  public void IBM2(List<SentencePair> trainingPairs) {
    CounterMap<String,String> targetSourceProbPrev = new CounterMap<String,String>();
    int iteration = 0;
    while (probDiff(targetSourceProbPrev, targetSourceProb) > IBM2_ITER_THRESHOLD || iteration <= 1) {      
      targetSourceProbPrev = targetSourceProb;
      CounterMap<String,String> probCount = new CounterMap<String,String>();
      CounterMap<String,Integer> alignCount = new CounterMap<String,Integer>();
      for (SentencePair pair : trainingPairs){
        List<String> targetWords = pair.getTargetWords();
        List<String> sourceWords = pair.getSourceWords();
        for(int sourceIdx = 0; sourceIdx < sourceWords.size(); sourceIdx++){
          String source = sourceWords.get(sourceIdx);
          Counter<String> tmpPairCount = new Counter<String>();
          Counter<Integer> tmpAlignCount = new Counter<Integer>();
          double total = 0.0;
          for (int targetIdx = -1; targetIdx < targetWords.size(); targetIdx++) {
            String target;
            if (targetIdx < 0) {
              target = NULL_WORD;
            } else {
              target = targetWords.get(targetIdx);
            }
            String alignKey = makeAlignKey(sourceIdx, sourceWords.size(), targetWords.size());
            double score = targetSourceProb.getCount(target, source) * alignProb.getCount(alignKey, targetIdx);
            tmpPairCount.incrementCount(target, score);
            tmpAlignCount.incrementCount(targetIdx, score);
            total += score;
          }
          for (int targetIdx = -1; targetIdx < targetWords.size(); targetIdx++) {
            String target;
            if (targetIdx < 0) {
              target = NULL_WORD;
            } else {
              target = targetWords.get(targetIdx);
            }
            String alignKey = makeAlignKey(sourceIdx, sourceWords.size(), targetWords.size());
            probCount.incrementCount(target, source, tmpPairCount.getCount(target) / total);
            alignCount.incrementCount(alignKey, targetIdx, tmpAlignCount.getCount(targetIdx) / total);
          }
        }
      } 
      targetSourceProb = Counters.conditionalNormalize(probCount);
      alignProb = Counters.conditionalNormalize(alignCount);
      
      iteration++;
      System.out.println("IBM2 " + String.valueOf(iteration) + " iter: " + String.valueOf(probDiff(targetSourceProbPrev, targetSourceProb)));
      if (System.currentTimeMillis() - startTime > TIME_LIMIT) {
        System.out.println("Time long. Terminate iteration.");
        break;
      }
    }
  }
  
  public double probDiff(CounterMap<String, String> a, CounterMap<String, String> b) {
    if (a.isEmpty() && b.isEmpty()) {
      return 0.0;
    }
    double rs = 0.0;
    Set<String> aKeys = a.keySet();
    Set<String> bKeys = b.keySet();
    for (String aKey: aKeys) {
      rs += probDiff(a.getCounter(aKey), b.getCounter(aKey));
    }
    for (String bKey: bKeys) {
      if (!aKeys.contains(bKey)) {
        rs += probDiff(a.getCounter(bKey), b.getCounter(bKey));
      }
    }
    return rs / Math.max(a.size(), b.size());
  }
  
  public double probDiff(Counter<String> a, Counter<String> b) {
    double rs = 0.0;
    for (String aKey: a.keySet()) {
      rs += (a.getCount(aKey) - b.getCount(aKey)) * (a.getCount(aKey) - b.getCount(aKey));
    }
    for (String bKey: b.keySet()) {
      if (!a.containsKey(bKey)) {
        rs += b.getCount(bKey) * b.getCount(bKey);
      }
    }
    return rs;
  }
  
  public String makeAlignKey(int sourceIdx, int sourceNum, int targetNum) {
    return String.valueOf(sourceNum) + "," + String.valueOf(targetNum) + "," +String.valueOf(sourceIdx);
  }
  
  public void showAlignProb() {
    for (String key: alignProb.keySet()) {
      Counter<Integer> counter = alignProb.getCounter(key);
      for (Integer targetIdx: counter.keySet()) {
        System.out.println(key + "," + String.valueOf(targetIdx) + ": " + String.valueOf(counter.getCount(targetIdx)));
      }
    }
  }
}
