package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;
import java.util.Set;

/**
 * Simple word alignment baseline model that maps source positions to target 
 * positions along the diagonal of the alignment grid.
 * 
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * 
 * @author Dan Klein
 * @author Spence Green
 */
public class IBMModel1Aligner implements WordAligner {

  private static final long serialVersionUID = 1315751943476440515L;
  private static double ITER_THRESHOLD = 5E-4;
  
  // TODO: Use arrays or Counters for collecting sufficient statistics
  // from the training data.
  private CounterMap<String, String> targetSourceProb;

  public Alignment align(SentencePair sentencePair) {
    // Placeholder code below. 
    // TODO Implement an inference algorithm for Eq.1 in the assignment
    // handout to predict alignments based on the counts you collected with train().
    Alignment alignment = new Alignment();

    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sourceWords.size();
    int numTargetWords = targetWords.size();
    for (int sourceIdx = 0; sourceIdx < numSourceWords; sourceIdx++) {
      String source = sourceWords.get(sourceIdx);
      double max = 0.0;
      int maxIdx = -1;
      for(int targetIdx = 0; targetIdx < numTargetWords; targetIdx++){
        String target = targetWords.get(targetIdx);
        double score = targetSourceProb.getCount(target, source);
        if (max < score) {
          max = score;
          maxIdx = targetIdx;
        }
      }
      if (maxIdx >= 0) {
        alignment.addPredictedAlignment(maxIdx, sourceIdx);
      }
    }
   
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
    // initialize
    targetSourceProb = new CounterMap<String,String>();
    for (SentencePair pair : trainingPairs){
      List<String> targetWords = pair.getTargetWords();
      List<String> sourceWords = pair.getSourceWords();
      for(String source : sourceWords){
        for(String target : targetWords){
          targetSourceProb.incrementCount(target, source, 1.0);
        }
        targetSourceProb.incrementCount(NULL_WORD, source, 1.0);
      }
    }
    
    // EM iteration
    CounterMap<String,String> targetSourceProbPrev = new CounterMap<String,String>();
    int iteration = 0;
    while (probDiff(targetSourceProbPrev, targetSourceProb) > ITER_THRESHOLD) {      
      targetSourceProbPrev = targetSourceProb;
      CounterMap<String,String> probCount = new CounterMap<String,String>();
      for (SentencePair pair : trainingPairs){
        List<String> targetWords = pair.getTargetWords();
        List<String> sourceWords = pair.getSourceWords();
        for(int sourceIdx = 0; sourceIdx < sourceWords.size(); sourceIdx++){
          String source = sourceWords.get(sourceIdx);
          Counter<String> tmpCount = new Counter<String>();
          double total = 0.0;
          for (String target: targetWords) {
            tmpCount.incrementCount(target, targetSourceProb.getCount(target, source));
            total += targetSourceProb.getCount(target, source);
          }
          tmpCount.incrementCount(NULL_WORD, targetSourceProb.getCount(NULL_WORD, source));
          total += targetSourceProb.getCount(NULL_WORD, source);
          for (String target: tmpCount.keySet()) {
            probCount.incrementCount(target, source, tmpCount.getCount(target) / total);
          }
        }
      } 
      
      targetSourceProb = Counters.conditionalNormalize(probCount);
      
      iteration++;
      System.out.println(String.valueOf(iteration) + " iter: " + String.valueOf(probDiff(targetSourceProbPrev, targetSourceProb)));
    } 

  }
  
  public double probDiff(CounterMap<String, String> a, CounterMap<String, String> b) {
    if (a.isEmpty() && b.isEmpty()) {
      return 0.0;
    }
    double rs = 0.0;
    Set<String> aKeys = a.keySet();
    Set<String> bKeys = b.keySet();
    for (String aKey: aKeys) {
      rs += probDiff(a.getCounter(aKey), b.getCounter(aKey));
    }
    for (String bKey: bKeys) {
      if (!aKeys.contains(bKey)) {
        rs += probDiff(a.getCounter(bKey), b.getCounter(bKey));
      }
    }
    return rs / Math.max(a.size(), b.size());
  }
  
  public double probDiff(Counter<String> a, Counter<String> b) {
    double rs = 0.0;
    for (String aKey: a.keySet()) {
      rs += (a.getCount(aKey) - b.getCount(aKey)) * (a.getCount(aKey) - b.getCount(aKey));
    }
    for (String bKey: b.keySet()) {
      if (!a.containsKey(bKey)) {
        rs += b.getCount(bKey) * b.getCount(bKey);
      }
    }
    return rs;
  }
  
}
