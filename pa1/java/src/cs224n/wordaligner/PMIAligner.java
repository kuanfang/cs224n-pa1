package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;

/**
 * Simple word alignment baseline model that maps source positions to target 
 * positions along the diagonal of the alignment grid.
 * 
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * 
 * @author Dan Klein
 * @author Spence Green
 */
public class PMIAligner implements WordAligner {

  private static final long serialVersionUID = 1315751943476440515L;
  
  // TODO: Use arrays or Counters for collecting sufficient statistics
  // from the training data.
  private CounterMap<String,String> sourceTargetCounts;
  private Counter<String> sourceCounts;
  private Counter<String> targetCounts;

  public Alignment align(SentencePair sentencePair) {
    // Placeholder code below. 
    // TODO Implement an inference algorithm for Eq.1 in the assignment
    // handout to predict alignments based on the counts you collected with train().
    Alignment alignment = new Alignment();

    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sourceWords.size();
    int numTargetWords = targetWords.size();

    for (int srcIndex = 0; srcIndex < numSourceWords; srcIndex++) {
      String src = sourceWords.get(srcIndex);
      Counter<String> keySrcCounter = sourceTargetCounts.getCounter(sourceWords.get(srcIndex));
      double max = 0.0;
      String maxStr = "";
      for (int tgtIndex = 0; tgtIndex < numTargetWords; tgtIndex++) {
        String tgt = targetWords.get(tgtIndex);
        if (sourceTargetCounts.getCount(src, tgt) > 0) {
          double p = sourceTargetCounts.getCount(src, tgt) / targetCounts.getCount(tgt);
          if (max < p) {
            max = p;
            maxStr = tgt;
          } 
        }
      }
      int matchIndex = targetWords.indexOf(maxStr);
      if (matchIndex >= 0) {
        alignment.addPredictedAlignment(matchIndex, srcIndex);
      }
    }
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
    
    sourceTargetCounts = new CounterMap<String,String>();
    sourceCounts = new Counter<String>();
    targetCounts = new Counter<String>();
    for(SentencePair pair : trainingPairs){
      List<String> targetWords = pair.getTargetWords();
      List<String> sourceWords = pair.getSourceWords();
      for(String source : sourceWords){
        for(String target : targetWords){
          sourceTargetCounts.incrementCount(source, target, 1.0);
          sourceCounts.incrementCount(source, 1.0);
          targetCounts.incrementCount(target, 1.0);
        }
        sourceTargetCounts.incrementCount(source, NULL_WORD, 1.0);
        sourceCounts.incrementCount(source, 1.0);
        targetCounts.incrementCount(NULL_WORD, 1.0);
      }
    }

  }
}
