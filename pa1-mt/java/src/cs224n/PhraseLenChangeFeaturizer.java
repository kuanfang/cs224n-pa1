package edu.stanford.nlp.mt.decoder.feat.sparse;

import java.util.Collections;
import java.util.List;

import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.util.Generics;

/*
*Target insertion ratio.
*
*@author Spence Green
*
*/
public class PhraseLenChangeFeaturizer implements RuleFeaturizer<IString, String> {
    private static final String FEATURE_NAME = "LRA";

    @Override
    public void initialize() {
          // Do any setup here.
    }
                  
    @Override
    public List<FeatureValue<String>> ruleFeaturize(
        Featurizable<IString, String> f) {
  
        List<FeatureValue<String>> features = Generics.newLinkedList();
        if (f.sourcePhrase.size() != 0) {
            features.add(new FeatureValue<String>(FEATURE_NAME, ((double) f.targetPhrase.size())/ ((double) f.sourcePhrase.size())));
        } 
        else {
            features.add(new FeatureValue<String>(FEATURE_NAME, 0));
        }
        return features;
    }

    @Override
    public boolean isolationScoreOnly() {
        return false;
    }
}

//    public static final String FEATURE_NAME = "PLC";
//
//    @Override
//    public void initialize() {}
//
//    @Override
//    public List<FeatureValue<String>> ruleFeaturize(
//        Featurizable<IString, String> f) {
//  
//        List<FeatureValue<String>> features = Generics.newLinkedList();
//        if (f.sourcePhrase.size() != 0) {
//            features.add(new FeatureValue<String>(FEATURE_NAME, ((double) f.targetPhrase.size())/ ((double) f.sourcePhrase.size())));
//        } 
//        else {
//            features.add(new FeatureValue<String>(FEATURE_NAME, 0));
//        }
//        return features;
//    }
//
//    //@Override
//    //public List<FeatureValue<String>> ruleFeaturize(Featurizable<IString, String> f) {
//    //    //int change = 0;
//    //    //for (int i = 0; i < f.targetPhrase.size(); ++i) {
//    //    //    int[] t2s = f.rule.abstractRule.alignment.t2s(i);
//    //    //    // if (t2s == null || t2s.length == 0) ++numInserted;
//    //    //    if (t2s == null) {
//    //    //        change += 1;
//    //    //    }
//    //    //    else {
//    //    //        change += Math.abs(t2s.length - 1);
//    //    //    }
//    //    //}
//    //    ////System.out.println("hello!");
//    //    ////System.out.println(change);
//    //    //final double ratio = (double) change / (double) f.sourceSentence.size();
//    //    ////return Collections.singletonList(new FeatureValue<>(FEATURE_NAME, ratio));
//    //    ////
//    //    //// List<FeatureValue<String>> features = new LinkedList<>();
//    //    List<FeatureValue<String>> features = Generics.newLinkedList();
//    //    //features.add(new FeatureValue<String>(FEATURE_NAME, ratio));
//    //    features.add(new FeatureValue<String>(FEATURE_NAME, 1.0));
//    //    return features;
//    //}
//
//    @Override
//    public boolean isolationScoreOnly() {
//        return false;
//    }
//}
//
