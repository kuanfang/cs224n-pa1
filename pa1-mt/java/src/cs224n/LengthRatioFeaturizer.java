package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;


/**
 * Length Ratio
 */
public class LengthRatioFeaturizer implements RuleFeaturizer<IString, String> {
      
    private static final String FEATURE_NAME = "LRA";

    @Override
    public void initialize() {
          // Do any setup here.
    }
                  
    @Override
    public List<FeatureValue<String>> ruleFeaturize(
        Featurizable<IString, String> f) {
  
        List<FeatureValue<String>> features = Generics.newLinkedList();
        if (f.sourcePhrase.size() != 0) {
            features.add(new FeatureValue<String>(FEATURE_NAME, ((double) f.targetPhrase.size())/ ((double) f.sourcePhrase.size())));
        } 
        else {
            features.add(new FeatureValue<String>(FEATURE_NAME, 0));
        }
        return features;
    }

    @Override
    public boolean isolationScoreOnly() {
        return false;
    }
}
