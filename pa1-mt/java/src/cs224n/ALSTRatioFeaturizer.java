package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;
import edu.stanford.nlp.mt.util.PhraseAlignment;

/**
 * A rule featurizer.
 */
public class ALSTRatioFeaturizer implements RuleFeaturizer<IString, String> {
  
  private static final String FEATURE_NAME = "My";
  
  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {
    List<FeatureValue<String>> features = Generics.newLinkedList();

    // Target and Source Distortion
    PhraseAlignment alignment = f.rule.abstractRule.alignment;
    int prevTargetAlign = 0;
    int prevSourceAlign = 0;
    int sourceDistort = 0;
    int targetDistort = 0;
    for (int targetAlign = 0; targetAlign < f.targetPhrase.size(); ++targetAlign) {
      int[] alignments = alignment.t2s(targetAlign);
      if (alignments == null) {
        continue;
      } else {
        for (int sourceAlign : alignments) {
          sourceDistort += Math.abs(sourceAlign-prevSourceAlign) >= 1 ? 1: 0;
          targetDistort += Math.abs(targetAlign-prevTargetAlign) >= 1 ? 1: 0;
          prevTargetAlign = targetAlign;
          prevSourceAlign = sourceAlign;
        }
      }
    }
    double sourceDistortRatio = sourceDistort / (double) f.sourceSentence.size();
    double targetDistortRatio = targetDistort / (double) f.sourceSentence.size();
    features.add(new FeatureValue<String>(FEATURE_NAME + "src", sourceDistortRatio));
    features.add(new FeatureValue<String>(FEATURE_NAME + "trg", targetDistortRatio));

    // 

    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}


