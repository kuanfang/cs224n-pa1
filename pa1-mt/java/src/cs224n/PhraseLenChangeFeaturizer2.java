package edu.stanford.nlp.mt.decoder.feat.sparse;

import java.util.Collections;
import java.util.List;

import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.util.Generics;

/*
*Target insertion ratio.
*
*@author Spence Green
*
*/
public class PhraseLenChangeFeaturizer2 implements RuleFeaturizer<IString, String> {
    public static final String FEATURE_NAME = "PLC";

    @Override
    public void initialize() {}

    @Override
    public List<FeatureValue<String>> ruleFeaturize(Featurizable<IString, String> f) {
        int change = 0;
        for (int i = 0; i < f.targetPhrase.size(); ++i) {
            int[] t2s = f.rule.abstractRule.alignment.t2s(i);
            // if (t2s == null || t2s.length == 0) ++numInserted;
            if (t2s == null) {
                change += 1;
            }
            else {
                change += Math.abs(t2s.length - 1);
            }
        }
        final double ratio = (double) change / (double) f.sourceSentence.size();
        List<FeatureValue<String>> features = Generics.newLinkedList();
        features.add(new FeatureValue<String>(FEATURE_NAME, ratio));
        return features;
    }

    @Override
    public boolean isolationScoreOnly() {
        return false;
    }
}


