package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;
import edu.stanford.nlp.mt.util.PhraseAlignment;
import edu.stanford.nlp.mt.util.TokenUtils;


/*
* A measure of how much punctuation should be translated.
*  
* @author Spence Green
* 
*/
public class PunctuationDifferenceFeaturizer implements RuleFeaturizer<IString, String> {

    private static final String FEATURE_NAME = "PDIF";
        
    @Override
    public void initialize() {
    // Do any setup here.
    }

    @Override
    public List<FeatureValue<String>> ruleFeaturize(
        Featurizable<IString, String> f) {
        int numSourcePunctuationTokens = 0;
        for (IString token : f.sourcePhrase) {
            if (TokenUtils.isPunctuation(token.toString())) {
                ++numSourcePunctuationTokens;
            }
        }

        if (numSourcePunctuationTokens == 0) return null;
        int numTargetPunctuationTokens = 0;
        for (IString token : f.targetPhrase) {
            if (TokenUtils.isPunctuation(token.toString())) {
                ++numTargetPunctuationTokens;
            }
        }
        // List<FeatureValue<String>> features = new LinkedList<>();
        List<FeatureValue<String>> features = Generics.newLinkedList();
        double featureValue = (double) numTargetPunctuationTokens / (double) numSourcePunctuationTokens;
        features.add(new FeatureValue<String>(FEATURE_NAME, featureValue));
        return features;
    }
              
  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
