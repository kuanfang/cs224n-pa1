package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.util.Sequence;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;


/**
 * A rule featurizer.
 */
public class PrepRatioPlusFeaturizer implements RuleFeaturizer<IString, String> {
  
  //some common prepositions that are either consistently translated
  //correctly (e.g., on or in) or often spuriously added (e.g., to, of)
  private static final String[] prepList = {"about", "above", "at", "by", "in", "for", "of", "to", "on"};

  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {

    List<FeatureValue<String>> features = Generics.newLinkedList();
    //List<Integer> counts = new ArrayList<Integer>();
    int[] counts = new int[prepList.length];
    int total = 5;
    int i = 0;
    for (String prep : prepList) {
        int count = 0;
        for (IString targetWord : f.targetPhrase) {
            if (prep.equals(targetWord.toString())) {
                count = count + 1; 
                total = total + 1;
            }
        }
        //counts.add(count);
        counts[i] = count;
        i = i + 1;
    }
    i = 0;
    for (String prep : prepList) {
        if (total != 0) {
            features.add(new FeatureValue<String>("PrepRatioPlus_" + prep.toUpperCase(), (double) counts[i] / (double) total));
        }
        i = i + 1;
    }
    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}


