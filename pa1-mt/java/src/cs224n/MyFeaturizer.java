package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;
import edu.stanford.nlp.mt.util.PhraseAlignment;

/**
 * A rule featurizer.
 */
public class MyFeaturizer implements RuleFeaturizer<IString, String> {
  
    private static final String FEATURE_NAME = "My2";
  
    @Override
    public void initialize() {
        // Do any setup here.
    }

    @Override
    public List<FeatureValue<String>> ruleFeaturize(
        Featurizable<IString, String> f) {
  
        List<FeatureValue<String>> features = Generics.newLinkedList();

        int change = 0;
        for (int i = 0; i < f.targetPhrase.size(); ++i) {
            int[] t2s = f.rule.abstractRule.alignment.t2s(i);
            // if (t2s == null || t2s.length == 0) ++numInserted;
            if (t2s == null) {
                change += 1;
            }
            else {
                change += Math.abs(t2s.length - 1);
            }
        }
        final double ratio = (double) change / (double) f.sourceSentence.size();
        features.add(new FeatureValue<String>(FEATURE_NAME, ratio));
        return features;
    }

    @Override
    public boolean isolationScoreOnly() {
        return false;
    }
}

