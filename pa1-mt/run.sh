#!/bin/bash
source setup.sh
path_project=~/courses/cs224n/cs224n-pa1/pa1-mt

# Train and Test
if [ "$1" == "t" ]; then
    cd $path_project/java
    ant
    cd $path_project/system
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
elif [ "$1" == "e" ]; then
    cd $path_project/java
    ant
    cd $path_project/system
    phrasal.sh cs224n.vars 1-5 myFeature.ini myfeature
elif [ "$1" == "tr" ]; then
    cd $path_project/java
    ant
    cd $path_project/system
    rm newstest2012.BLEU
    cd $path_project/system
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
elif [ "$1" == "t5" ]; then
    cd $path_project/java
    ant
    cd $path_project/system
    rm newstest2012.BLEU
    cd $path_project/system
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    head newstest2012.BLEU
    java edu.stanford.nlp.mt.tools.PrintWeights newstest2011.myfeature.online.final.binwts
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    head newstest2012.BLEU
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
elif [ "$1" == "t10" ]; then
    cd $path_project/java
    ant
    cd $path_project/system
    rm newstest2012.BLEU
    cd $path_project/system
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    head newstest2012.BLEU
    java edu.stanford.nlp.mt.tools.PrintWeights newstest2011.myfeature.online.final.binwts
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    head newstest2012.BLEU
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
    phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
# else if [ "$1" == "eval" ]; then
#     cd $path_project/java
#     ant
#     cd $path_project/system
#     phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
#     phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
#     phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
#     phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
#     phrasal.sh cs224n.vars 2,4-5 myFeature.ini myfeature
fi

# Print Out Results
    cd $path_project/system

    echo $'\n# Translation Samples:'
    head newstest2012.newstest2011.myfeature.trans

    echo $'\n# BLEU:'
    head newstest2012.BLEU

    echo $'\n# Feature Weights:'
    java edu.stanford.nlp.mt.tools.PrintWeights newstest2011.myfeature.online.final.binwts

